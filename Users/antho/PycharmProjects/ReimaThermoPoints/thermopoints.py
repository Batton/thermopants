from flask import Flask
from flask import jsonify
from flask import request as fr
from neo4j.v1 import GraphDatabase
import requests as req
import math

app = Flask(__name__)

api_key = "HZuw0xEj37RBeyBvWdzModPd2AChWymE"

uri = "bolt://35.230.155.94:7687"


@app.route('/Thermopoints')
def get_thermopoints():
    latitude = str(fr.args.get('latitude'))
    longditude = str(fr.args.get('longditude'))

    coordinates = latitude + ',' + longditude

    payload_location = {'apikey': api_key, 'q': coordinates}

    accuweather_location_url = 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search'

    # Get call the AccuWeather API to get the current location

    location = req.get(accuweather_location_url, params=payload_location)

    location_data = location.json()

    status = location.status_code

    location_data = location.json()

    location_key = location_data['Key']

    # Use key in second AccuWeather call

    accuweather_weather_url = 'http://dataservice.accuweather.com/currentconditions/v1/' + str(location_key)

    payload_weather = {'apikey': api_key, 'details': 'true'}

    weather = req.get(accuweather_weather_url, params=payload_weather)

    # Convert json to dictionary
    weather_json = weather.json()

    max_temperature_past_24_hours = weather_json[0]['TemperatureSummary']['Past24HourRange']['Maximum']['Metric'][
        'Value']
    temperature_unit = weather_json[0]['TemperatureSummary']['Past24HourRange']['Maximum']['Metric']['Unit']
    thermopoints = {
        "Max Temp last 24h": max_temperature_past_24_hours,
        "Temperature unit": temperature_unit,
        "Base Layer Set Thermopoints": 2,
        "Base Layer Top or Bottom Thermopoints": 1,

        "Middle layer": 4,
        "Outer Layer": 4,
    }

    driver = GraphDatabase.driver(uri, auth=("tony", "Bilot123"))

    #Outerwear
    outerware_cypher_query = "MATCH (node) WHERE node.ThermoPoints = {thermopoints} AND (node.InsulationType = 'Outerwear') Return node.ProductNumber"

    thermopoints = thermopoints_for_temperature('Outerwear', max_temperature_past_24_hours)

    outerware_layer_products = get_products_for_thermopoints(driver, thermopoints, outerware_cypher_query)

    #Outerwear set/overall
    outerware_set_cypher_query = "MATCH (node) WHERE node.ThermoPoints = {thermopoints} AND (node.InsulationType = 'Outerwear set/overall') Return node.ProductNumber"

    thermopoints = thermopoints_for_temperature('Outerwear set/overall', max_temperature_past_24_hours)

    outerware_layer_set_products = get_products_for_thermopoints(driver, thermopoints, outerware_set_cypher_query)

    # Mid layer
    middle_layer_cypher_query = "MATCH (node) WHERE node.ThermoPoints = {thermopoints} AND (node.InsulationType = 'Mid layer') Return node.ProductNumber"

    thermopoints = thermopoints_for_temperature('Mid layer', max_temperature_past_24_hours)

    middle_layer_products = get_products_for_thermopoints(driver, thermopoints, middle_layer_cypher_query)

    #Mid layer set/overall

    middle_layer_set_cypher_query = "MATCH (node) WHERE node.ThermoPoints = {thermopoints} AND (node.InsulationType = 'Mid layer set/overall') Return node.ProductNumber"

    thermopoints = thermopoints_for_temperature('Mid layer set/overall', max_temperature_past_24_hours)

    middle_layer_set_products = get_products_for_thermopoints(driver, thermopoints, middle_layer_set_cypher_query)

    #Base layer

    base_layer_cypher_query = "MATCH (node) WHERE node.ThermoPoints = {thermopoints} AND (node.InsulationType = 'Base layer') Return node.ProductNumber"

    thermopoints = thermopoints_for_temperature('Base layer', max_temperature_past_24_hours)

    base_layer_products = get_products_for_thermopoints(driver, thermopoints, base_layer_cypher_query)

    #Base layer set/overall

    base_layer_set_cypher_query = "MATCH (node) WHERE node.ThermoPoints = {thermopoints} AND (node.InsulationType = 'Base layer set/overall') Return node.ProductNumber"

    thermopoints = thermopoints_for_temperature('Base layer set/overall', max_temperature_past_24_hours)

    base_layer_set_products = get_products_for_thermopoints(driver, thermopoints, base_layer_set_cypher_query)


    clothing = {}
    clothing["Base layer products"] = base_layer_products
    clothing["Base layer set/overall products"] = base_layer_set_products
    clothing["Middle Layer products"] = middle_layer_products
    clothing["Middle Layer set/overall products"] = middle_layer_set_products
    clothing["Outerwear products"] = outerware_layer_products
    clothing["Outerwear set/overall products"] = outerware_layer_set_products

    return jsonify(clothing)


def thermopoints_for_temperature(layer, temperature):
    temperature = math.floor(temperature)
    if layer == 'Base layer':
        return 1
    elif layer == 'Base layer set/overall':
        return 2
    elif layer == 'Mid layer':
        return math.floor((-0.0889 * temperature + 1.3333)/2)
    elif layer == 'Mid layer set/overall':
        return math.floor(-0.0889 * temperature + 1.3333)
    elif layer == 'Outerwear':
        return math.floor((-0.1111 * temperature + 5.6667)/2)
    elif layer == 'Outerwear set/overall':
        return math.floor(-0.1111 * temperature + 5.6667)



def get_products_for_thermopoints(driver, thermopoints, cypher_query):
    with driver.session() as session:
        with session.begin_transaction() as tx:
            clothing = []
            for record in tx.run(cypher_query, thermopoints=thermopoints):

                clothing.append(record["node.ProductNumber"])

            return clothing


